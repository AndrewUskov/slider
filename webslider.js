var slider = {
    config: {
        index: 0,
        loopIntreval: 5000,
        loop: null,
        startLoop: false,
        stopLoop: true,
        widthScreen: '1858',
        image: '.images>ul>li>img',
        classForSlide: 'forSlide',
        content: {
            current: 'li.current',
            previous: 'li.previous',
            next: 'li.next'
        },
        dots: {
            indexPoint: null,
            dot: '.owl-dot',
            dotActive: 'owl-dot-active'
        },
        slick: {
            beginCord: null,
            endCord: null,
            resultCord: null,
            invisible: 'div.invisible',
            invisibleSlick: 'invisibleSlick'
        }
    },

    start: function () {
        $(this.config.image).eq(this.config.index).clone().addClass(this.config.classForSlide).
        appendTo($(this.config.content.current));
        $(this.config.dots.dot).eq(this.config.index).addClass(this.config.dots.dotActive);
    },

    nextStart: function () {
        $(this.config.image).eq(this.config.index).clone().addClass(this.config.classForSlide).
        appendTo($(this.config.content.next)).animate({left: '-=' + this.config.widthScreen}, 1000);
    },

    previousStart: function () {
        $(this.config.image).eq(this.config.index).clone().addClass(this.config.classForSlide).
        appendTo($(this.config.content.previous)).animate({left: '+=' + this.config.widthScreen}, 1000);
    },

    removeLeft: function () {
        $('.' + this.config.classForSlide).animate({left: '-=' + this.config.widthScreen}, 1000,
            function () {$(this).remove();});
    },

    removeRight: function () {
        $('.' + this.config.classForSlide).animate({left: '+=' + this.config.widthScreen}, 1000,
            function () {$(this).remove();});

    },

    moveLeft: function () {
        this.removeLeft();
        this.config.index = this.config.index + 1;
        if(this.config.index < $(this.config.image).length) {
            this.colorPoint();
            this.nextStart();
        } else {
            this.config.index = 0;
            this.colorPoint();
            this.nextStart();
        }
    },

    moveRight: function () {
        this.removeRight();
        this.config.index = this.config.index - 1;
        if(this.config.index  >= 0) {
            this.colorPoint();
            this.previousStart();
        } else {
            this.config.index = $(this.config.image).length - 1;
            this.colorPoint();
            this.previousStart();
        }
    },

    colorPoint: function() {
        $(this.config.dots.dot).removeClass(this.config.dots.dotActive);
        $(this.config.dots.dot).eq(this.config.index).addClass(this.config.dots.dotActive);
    },

    dotsMoveLeft: function () {
        this.removeLeft();
        this.config.index = this.config.dots.indexPoint;
        this.colorPoint();
        this.nextStart();
    },

    dotsMoveRight: function () {
        this.removeRight();
        this.config.index = this.config.dots.indexPoint;
        this.colorPoint();
        this.previousStart();
    },

    dotsMove: function (indexPoint) {
        this.config.dots.indexPoint = indexPoint;
        if (this.config.dots.indexPoint != this.config.index) {
            (this.config.dots.indexPoint > this.config.index) ? this.dotsMoveLeft() : this.dotsMoveRight();
        }
    },

    run: function () {
        var self = this;
        if(self.config.startLoop) {
            self.config.loop = setInterval(function () {
                self.moveLeft();
            }, self.config.loopIntreval);
        }
    },

    stop: function () {
        var self = this.config;
        if(self.stopLoop) {
            clearInterval(self.loop);
        }
    },

    beginSlick: function (beginCord) {
        $(this.config.slick.invisible).addClass(this.config.slick.invisibleSlick);
        this.config.slick.beginCord = beginCord;
        return this.config.slick.beginCord;
    },

    endSlick: function (endCord) {
        this.config.slick.endCord = endCord;
        this.config.slick.resultCord = this.config.slick.endCord - this.config.slick.beginCord;
        if(this.config.slick.resultCord > 80) {
            this.moveRight();
        } else if ((this.config.slick.resultCord < (-80))) {
            this.moveLeft();
        };
        $(this.config.slick.invisible).removeClass(this.config.slick.invisibleSlick);
    }
};

$(document).ready(function () {
    slider.start();
    slider.run();
    $('span.moveRight').bind('click', function () {
        slider.moveRight();
    });
    $('span.moveLeft').bind('click', function () {
        slider.moveLeft();
    });
    $('.invisible, span').bind('mouseover', function () {
        slider.stop();
    });
    $('.invisible, span').bind('mouseout', function () {
        slider.run();
    });
    $('.owl-dot').bind('click', function () {
        var indexPoint = $(this).index();
        slider.dotsMove(indexPoint);
    });
    $('.invisible').bind('mousedown', function (event) {
        var beginCord = event.pageX;
        slider.beginSlick(beginCord);
    });
    $('.invisible').bind('mouseup', function (event) {
        var endCord = event.pageX;
        slider.endSlick(endCord);
    });
});



